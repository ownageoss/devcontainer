const globals = require("globals");
const pluginJs = require("@eslint/js");
const html = require('eslint-plugin-html');

module.exports = [
	pluginJs.configs.recommended,
	{
		languageOptions: { globals: globals.browser },
	},
	{
		files: ['**/*.html'],
		plugins: { html },
	},
	{
		rules: {
			"no-undef": "off"
		}
	}
];