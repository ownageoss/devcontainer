# DEV Container

The VS Code extension `ms-vscode-remote.remote-containers` lets you use a docker container as a dev environment.

The main advantage is that the dev container standardizes the tools, versions of packages, etc. so that developers have a consistent and fully featured dev environment.

## Base Image

Ownage will build a new multi architecture base image when there is a new release of go, rust or when software components need to be updated.

The following architectures are currently supported:

- linux/amd64
- linux/arm64

Example of base image tags:

- registry.gitlab.com/ownageoss/base-images:devcontainer-latest
- registry.gitlab.com/ownageoss/base-images:devcontainer-24.4.1 (yy.mm.count)

## How to use the DEV container

Pre-requisites:

- Docker installed on your machine.
- VS Code with the extension `ms-vscode-remote.remote-containers`.

Assuming your project directory looks something like this:

```
├── myproject
│   ├── app1
│   ├── app2
```

... run the following commands.

```sh
# Linux
cd myproject
git clone https://gitlab.com/ownageoss/devcontainer.git
mkdir .devcontainer
cp devcontainer/devcontainer.json .devcontainer/
```

Your folder structure should look similar to this:

```
├── myproject
│   ├── .devcontainer
│   │   ├── devcontainer.json
│   ├── app1
│   ├── app2
```

Open the folder `myproject` in VS Code and you should be prompted to open the current workspace as a dev container.

## devcontainer.json

This json file contains the config for the DEV container.

The base image and the settings are mostly tuned for a Go, Rust, K8S and gRPC based dev setup.

## Updates

From time to time, the DEV container's config will be updated.

Run the following commands to upgrade the dev container.

```sh
# Make sure you are running the dev container
cd /workspaces/myproject/devcontainer
git pull
cp devcontainer.json ../.devcontainer/

# Linux: F1 and run Dev Containers: Rebuild Container
# Mac OS: CMD + SHIFT + P and run Dev Containers: Rebuild Container
```







